# pyBOSARIS <br/><small>// working title</small>
code of research-industrial project, license: LGPLv3

<i>authors: <br/>
Andreas Nautsch (EURECOM)<br/>
Themos Stafylakis (Omilia)</i>

### Goal of this code
To provide a score normalization, calibration, fusion and performance evaluation suite. 
<br/>(unit tests are anticipated.)

Motivated by the sidekit implementation of the BOSARIS toolkit, and the forensic validation toolbox.

### Indication of changes
15 Jul, 2015 - today
<br/><a href="https://git-lium.univ-lemans.fr/Larcher/sidekit" target="black">sidekit</a> (maintainer: Anthony Larcher)
<br/><small>// conversion MATLAB to Python; solely regarding DET</small>

23 Oct, 2015 - 20 Sep, 2017
<br/><a href="https://sites.google.com/site/validationtoolbox/" target="black">Validation Toolbox</a> (maintainer: Rudolf Haraksim)
<br/><small>// LLR performance metrics & visualizations (e.g., Tippet & ECE plots)</small>

22 Mar, 2019 - today
<br/>pyBOSARIS (maintainer: Andreas Nautsch)
<br/><small>// standalone pkg; conversion to Python: calibration & fusion</small>
