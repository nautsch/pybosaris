from unittest import TestCase
from pybosaris import Quality, Key
import numpy
import os


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


testfile_txt = 'testfile_qual.txt'
testfile_h5 = 'testfile_qual.h5'
testfile_matlab = 'testfile_qual.hdf5'

key = Key()
key.modelset = numpy.array(['a', 'b', 'c'])
key.segset = numpy.array(['X', 'Y', 'Z', 'U'])
key.tar = numpy.array([[1, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]], dtype=bool)
key.non = ~key.tar
key.non[2, 3] = False  # unscored
assert(key.validate())


class TestQuality(TestCase):
    def setUp(self):
        self.qual = Quality()
        self.qual.modelset = numpy.array(['a', 'b', 'c'])
        self.qual.segset = numpy.array(['X', 'Y', 'Z', 'U'])
        self.qual.scoremask = numpy.array([[1, 1, 0,1], [1,1,1,1],[1,1,1,1]],dtype=bool)
        self.qual.modelQ = numpy.random.rand(3, 5)
        self.qual.segQ = numpy.random.rand(4, 5)
        self.qual.hasmodel = numpy.array([True, False, True])
        self.qual.hasseg = numpy.array([True, True, False, True])
        assert(self.qual.validate())

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(testfile_txt):
            os.remove(testfile_txt)
        if os.path.exists(testfile_h5):
            os.remove(testfile_h5)
        if os.path.exists(testfile_matlab):
            os.remove(testfile_matlab)
    
    def test_write_read_h5(self):
        self.qual.write(testfile_h5)
        qual2 = Quality.read(testfile_h5)
        qual3 = Quality(testfile_h5)
        self.assertEqual(self.qual, qual2)
        self.assertEqual(self.qual, qual3)

    """
    def test_write_read_txt(self):
        self.qual.write_txt(testfile_txt)
        qual2 = Quality.read_txt(testfile_txt)
        self.assertEqual(self.qual, qual2)
    """

    def test_write_read_matlab(self):
        self.qual.write_matlab(testfile_matlab)
        qual2 = Quality.read_matlab(testfile_matlab)
        self.assertEqual(self.qual, qual2)

    def test_align_with_ndx(self):
        ndx = key.to_ndx()
        ndx.modelset = ndx.modelset[[1,2,0]]
        ndx.trialmask = ndx.trialmask[[1,2,0], :]
        qual2 = self.qual.align_with_ndx(ndx)
        self.assertTrue((qual2.modelset == ndx.modelset).all())
        self.assertFalse((self.qual.modelset == ndx.modelset).all())
        qual3 = self.qual.align_with_ndx(key)
        self.assertTrue((qual3.modelset == key.modelset).all())
        self.assertTrue((self.qual.modelset == qual3.modelset).all())
        self.assertTrue((self.qual.modelQ == qual3.modelQ)[qual3.hasmodel,:].all())
        self.assertTrue((self.qual.segQ == qual3.segQ)[qual3.hasseg,:].all())
        self.assertTrue((self.qual.hasmodel == qual3.hasmodel).all())
        self.assertTrue((self.qual.hasseg == qual3.hasseg).all())

    def test_validate(self):
        self.assertTrue(self.qual.validate())
        qual2 = self.qual
        qual2.modelset = self.qual.modelset[1:]
        self.assertFalse(qual2.validate())
        qual2.modelset = self.qual.modelset
        qual2.segset = self.qual.segset[1:]
        self.assertFalse(qual2.validate())
        qual2.segset = self.qual.segset
        qual2.scoremask = self.qual.scoremask[1:,:]
        self.assertFalse(qual2.validate())
        qual2.scoremask = self.qual.scoremask
        qual2.modelQ = self.qual.modelQ[1:,:]
        self.assertFalse(qual2.validate())
        qual2.modelQ = self.qual.modelQ
        qual2.segQ = self.qual.segQ[1:,:]
        self.assertFalse(qual2.validate())
        qual2.segQ = self.qual.segQ
        qual2.hasmodel = self.qual.hasmodel[1:]
        self.assertFalse(qual2.validate())
        qual2.hasmodel = self.qual.hasmodel
        qual2.hasseg = self.qual.hasseg[1:]
        self.assertFalse(qual2.validate())

    """
    def test_merge(self):
        qual2 = Quality()
        qual2.modelset = numpy.array(['.',',',';'])
        qual2.segset = numpy.array(['1','2'])
        qual2.scoremask = numpy.random.rand(3,2) > 0.2
        qual2.modelQ = numpy.random.rand(3,5)
        qual2.segQ = numpy.random.rand(2,5)
        qual2.hasmodel = numpy.random.rand(3) < 0.8
        qual2.hasseg = numpy.random.rand(2) < 0.8
        assert(qual2.validate())

        qual3 = Quality.merge([self.qual, qual2])
        self.assertTrue(qual3.validate())
        self.assertEqual(set(qual3.modelset), set(self.qual.modelset).union(set(qual2.modelset)))
        self.assertEqual(set(qual3.segset), set(self.qual.segset).union(set(qual2.segset)))
    """
