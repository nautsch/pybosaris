from unittest import TestCase
from pybosaris import IdMap
import numpy
import copy
import os


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


testfile_txt = 'testfile_idmap.txt'
testfile_h5 = 'testfile_idmap.h5'
testfile_matlab = 'testfile_idmap.hdf5'


class TestIdMap(TestCase):
    def setUp(self):
        self.idmap = IdMap()
        self.idmap.leftids = numpy.array(['a', 'b', 'c'])
        self.idmap.rightids = numpy.array(['X', 'Y', 'Z'])
        self.idmap.start = numpy.empty(3, dtype='|O')
        self.idmap.stop = numpy.empty(3, dtype='|O')
        assert(self.idmap.validate())

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(testfile_txt):
            os.remove(testfile_txt)
        if os.path.exists(testfile_h5):
            os.remove(testfile_h5)
        if os.path.exists(testfile_matlab):
            os.remove(testfile_matlab)

    def test_write_read_h5(self):
        self.idmap.write(testfile_h5)
        idmap2 = IdMap().read(testfile_h5)
        idmap3 = IdMap(idmap_filename=testfile_h5)
        self.assertEqual(self.idmap,idmap2)
        self.assertEqual(self.idmap,idmap3)

    def test_write_read_txt(self):
        self.idmap.write_txt(testfile_txt)
        idmap2 = IdMap.read_txt(testfile_txt)
        self.assertEqual(self.idmap, idmap2)

    def test_map_left_to_right(self):
        rightids = self.idmap.map_left_to_right(self.idmap.leftids)
        self.assertTrue((rightids == self.idmap.rightids).all())
        rightids2 = self.idmap.map_left_to_right(self.idmap.leftids[:3])
        self.assertTrue((rightids2 == self.idmap.rightids[:3]).all())

    def test_map_right_to_left(self):
        leftids = self.idmap.map_right_to_left(self.idmap.rightids)
        self.assertTrue((leftids == self.idmap.leftids).all())
        leftids2 = self.idmap.map_right_to_left(self.idmap.rightids[:3])
        self.assertTrue((leftids2 == self.idmap.leftids[:3]).all())

    def test_filter_on_left(self):
        keep=False
        idmap2 = self.idmap.filter_on_left(self.idmap.leftids,keep=keep)
        self.assertTrue(idmap2.leftids.size == 0)
        self.assertTrue(idmap2.rightids.size == 0)
        self.assertTrue(idmap2.start.size == 0)
        self.assertTrue(idmap2.stop.size == 0)
        self.assertNotEqual(idmap2, self.idmap)
        idmap3 = self.idmap.filter_on_left(self.idmap.leftids[1:],keep=keep)
        idmap4 = IdMap()
        idmap4.leftids = self.idmap.leftids[1:]
        idmap4.rightids = self.idmap.rightids[1:]
        idmap4.start = self.idmap.start[1:]
        idmap4.stop = self.idmap.stop[1:]
        assert(idmap4.validate())
        self.assertEqual(idmap3.leftids,self.idmap.leftids[0])
        self.assertEqual(idmap3.rightids,self.idmap.rightids[0])
        self.assertEqual(idmap3.start,self.idmap.start[0])
        self.assertEqual(idmap3.stop,self.idmap.stop[0])
        self.assertNotEqual(idmap3,idmap4)

        keep=True
        idmap2 = self.idmap.filter_on_left(self.idmap.leftids,keep=keep)
        self.assertEqual(idmap2, self.idmap)
        idmap3 = self.idmap.filter_on_left(self.idmap.leftids[1:],keep=keep)
        idmap4 = IdMap()
        idmap4.leftids = self.idmap.leftids[1:]
        idmap4.rightids = self.idmap.rightids[1:]
        idmap4.start = self.idmap.start[1:]
        idmap4.stop = self.idmap.stop[1:]
        assert(idmap4.validate())
        self.assertEqual(idmap3,idmap4)

    def test_filter_on_right(self):
        keep=False
        idmap2 = self.idmap.filter_on_right(self.idmap.rightids,keep=keep)
        self.assertTrue(idmap2.leftids.size == 0)
        self.assertTrue(idmap2.rightids.size == 0)
        self.assertTrue(idmap2.start.size == 0)
        self.assertTrue(idmap2.stop.size == 0)
        self.assertNotEqual(idmap2, self.idmap)
        idmap3 = self.idmap.filter_on_right(self.idmap.rightids[1:],keep=keep)
        idmap4 = IdMap()
        idmap4.leftids = self.idmap.leftids[1:]
        idmap4.rightids = self.idmap.rightids[1:]
        idmap4.start = self.idmap.start[1:]
        idmap4.stop = self.idmap.stop[1:]
        assert(idmap4.validate())
        self.assertEqual(idmap3.leftids,self.idmap.leftids[0])
        self.assertEqual(idmap3.rightids,self.idmap.rightids[0])
        self.assertEqual(idmap3.start,self.idmap.start[0])
        self.assertEqual(idmap3.stop,self.idmap.stop[0])
        self.assertNotEqual(idmap3,idmap4)

        keep=True
        idmap2 = self.idmap.filter_on_right(self.idmap.rightids,keep=keep)
        self.assertEqual(idmap2, self.idmap)
        idmap3 = self.idmap.filter_on_right(self.idmap.rightids[1:],keep=keep)
        idmap4 = IdMap()
        idmap4.leftids = self.idmap.leftids[1:]
        idmap4.rightids = self.idmap.rightids[1:]
        idmap4.start = self.idmap.start[1:]
        idmap4.stop = self.idmap.stop[1:]
        assert(idmap4.validate())
        self.assertEqual(idmap3,idmap4)

    def test_validate(self):
        self.assertTrue(self.idmap.validate())
        idmap2 = IdMap()
        idmap2.leftids = self.idmap.leftids
        idmap2.rightids = self.idmap.rightids
        idmap2.start = self.idmap.start
        idmap2.stop = self.idmap.stop
        self.assertTrue(idmap2.validate())
        idmap2.leftids = self.idmap.leftids[1:]
        self.assertFalse(idmap2.validate())
        idmap2.leftids = self.idmap.leftids
        idmap2.rightids = self.idmap.rightids[1:]
        self.assertFalse(idmap2.validate())
        idmap2.rightids = self.idmap.rightids
        idmap2.start = self.idmap.start[1:]
        self.assertFalse(idmap2.validate())
        idmap2.start = self.idmap.start
        idmap2.stop = self.idmap.stop[1:]
        self.assertFalse(idmap2.validate())
        idmap2.stop = self.idmap.stop
        idmap2.rightids = numpy.concatenate((self.idmap.rightids,self.idmap.leftids))
        self.assertFalse(idmap2.validate())
        idmap2.rightids = self.idmap.rightids
        idmap2.leftids = numpy.concatenate((self.idmap.rightids,self.idmap.leftids))
        self.assertFalse(idmap2.validate())

    def test_set(self):
        leftids = copy.deepcopy(self.idmap.leftids)
        rightids = copy.deepcopy(self.idmap.rightids)
        self.idmap.set(left=leftids[:2],right=rightids[:2])
        self.assertTrue((self.idmap.leftids == leftids[:2]).all())
        self.assertTrue((self.idmap.rightids == rightids[:2]).all())
        self.idmap.set(left=leftids,right=rightids)
        self.assertTrue((self.idmap.leftids == leftids).all())
        self.assertTrue((self.idmap.rightids == rightids).all())

    def test_merge(self):
        idmap2 = IdMap()
        idmap2.leftids = numpy.array(['1', '2'])
        idmap2.rightids = numpy.array([';', ','])
        idmap2.start = numpy.empty(2,dtype='|O')
        idmap2.stop = numpy.empty(2,dtype='|O')
        assert(idmap2.validate())

        idmap3 = self.idmap.merge(idmap2)
        self.assertEqual(set(idmap3.leftids), set(self.idmap.leftids).union(set(idmap2.leftids)))
        self.assertEqual(set(idmap3.rightids), set(self.idmap.rightids).union(set(idmap2.rightids)))
        self.assertEqual(set(idmap3.start), set(self.idmap.start).union(set(idmap2.start)))
        self.assertEqual(set(idmap3.stop), set(self.idmap.stop).union(set(idmap2.stop)))
