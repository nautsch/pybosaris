from unittest import TestCase
from pybosaris.libperformance import effective_prior, logit_effective_prior, cllr, min_cllr, fast_minDCF, rocch, fast_actDCF
from scipy.special import logit
import numpy
from tests.test_libmath import y1, y2, y3, y4


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


class TestLibPerformance(TestCase):
    def test_effective_prior(self):
        self.assertEqual(effective_prior(Ptar=0.01,cmiss=1,cfa=1), 0.01)
        self.assertEqual(effective_prior(Ptar=0.5,cmiss=1,cfa=99), 0.01)
        self.assertEqual(effective_prior(Ptar=0.5,cmiss=99,cfa=1), 0.99)
        self.assertEqual(effective_prior(Ptar=0.01,cmiss=99,cfa=1), 0.5)
        self.assertEqual(effective_prior(Ptar=0.5,cmiss=1,cfa=100), 1/101)
        self.assertEqual(effective_prior(Ptar=0.5,cmiss=10,cfa=1), 10/11)
        self.assertEqual(effective_prior(Ptar=0.5,cmiss=1,cfa=10), 1/11)

    def test_logit_effective_prior(self):
        self.assertTrue(logit_effective_prior(Ptar=0.01,cmiss=1,cfa=1) - logit(0.01) <= 1e-15)
        self.assertTrue(logit_effective_prior(Ptar=0.5,cmiss=1,cfa=99) - logit(0.01) <= 1e-15)
        self.assertTrue(logit_effective_prior(Ptar=0.5,cmiss=99,cfa=1) - logit(0.99) <= 1e-15)
        self.assertTrue(logit_effective_prior(Ptar=0.01,cmiss=99,cfa=1) - logit(0.5) <= 1e-15)
        self.assertTrue(logit_effective_prior(Ptar=0.5,cmiss=1,cfa=100) - logit(1/101) <= 1e-15)
        self.assertTrue(logit_effective_prior(Ptar=0.5,cmiss=10,cfa=1) - logit(10/11) <= 1e-15)
        self.assertTrue(logit_effective_prior(Ptar=0.5,cmiss=1,cfa=10) - logit(1/11) <= 1e-15)

    def test_rocch(self):
        s1 = numpy.arange(len(y1))
        s2 = numpy.arange(len(y2))
        s3 = numpy.arange(len(y3))
        s4 = numpy.arange(len(y4))
        self.assertTrue((numpy.stack(rocch(s1[y1], s1[~y1])) - numpy.stack([
            numpy.array([0,0,0.714285714285714,1]),
            numpy.array([1,0.800000000000000,0,0])
        ]) < 1e-15).all())
        self.assertTrue((numpy.stack(rocch(s2[y2], s2[~y2])) - numpy.stack([
            numpy.array([0,1]),
            numpy.array([1,0])
        ]) < 1e-15).all())
        self.assertTrue((numpy.stack(rocch(s3[y3], s3[~y3])) - numpy.stack([
            numpy.array([0,0,0.400000000000000,0.800000000000000,1]),
            numpy.array([1,0.625000000000000,0.125000000000000,0,0])
        ]) < 1e-15).all())
        self.assertTrue((numpy.stack(rocch(s4[y4], s4[~y4])) - numpy.stack([
            numpy.array([0,0.214285714285714,0.285714285714286,0.785714285714286,1]),
            numpy.array([1,0.363636363636364,0.272727272727273,0,0])
        ]) < 1e-15).all())

    def test_fast_minDCF_eer(self):
        plo = logit(0.01)
        s1 = numpy.arange(len(y1))
        s2 = numpy.arange(len(y2))
        s3 = numpy.arange(len(y3))
        s4 = numpy.arange(len(y4))

        minDCF, Pmiss, Pfa, prbep, eer = fast_minDCF(s1[y1], s1[~y1], plo, normalize=True)
        self.assertTrue(numpy.abs(minDCF - 0.714285714285714302) < 1e-15)
        self.assertTrue(numpy.abs(Pmiss[0] - 0.714285714285714302) < 1e-15)
        self.assertTrue(numpy.abs(Pfa[0] - 0.000000000000000000) < 1e-15)
        self.assertTrue(numpy.abs(prbep - 3.076923076923076650) < 1e-15)
        self.assertTrue(numpy.abs(eer - 0.377358490566037763) < 1e-15)

        minDCF, Pmiss, Pfa, prbep, eer = fast_minDCF(s2[y2], s2[~y2], plo, normalize=True)
        self.assertTrue(numpy.abs(minDCF - 1.000000000000000000) < 1e-15)
        self.assertTrue(numpy.abs(Pmiss[0] - 1.000000000000000000) < 1e-15)
        self.assertTrue(numpy.abs(Pfa[0] - 0.000000000000000000) < 1e-15)
        self.assertTrue(numpy.abs(prbep - 3.076923076923076650) < 1e-15)
        self.assertTrue(numpy.abs(eer - 0.500000000000000000) < 1e-15)

        minDCF, Pmiss, Pfa, prbep, eer = fast_minDCF(s3[y3], s3[~y3], plo, normalize=True)
        self.assertTrue(numpy.abs(minDCF - 0.800000000000000044) < 1e-15)
        self.assertTrue(numpy.abs(Pmiss[0] - 0.800000000000000044) < 1e-15)
        self.assertTrue(numpy.abs(Pfa[0] - 0.000000000000000000) < 1e-15)
        self.assertTrue(numpy.abs(prbep - 1.666666666666666519) < 1e-15)
        self.assertTrue(numpy.abs(eer - 0.277777777777777790) < 1e-15)

        minDCF, Pmiss, Pfa, prbep, eer = fast_minDCF(s4[y4], s4[~y4], plo, normalize=True)
        self.assertTrue(numpy.abs(minDCF - 0.785714285714285587) < 1e-15)
        self.assertTrue(numpy.abs(Pmiss[0] - 0.785714285714285698) < 1e-15)
        self.assertTrue(numpy.abs(Pfa[0] - 0.000000000000000000) < 1e-15)
        self.assertTrue(numpy.abs(prbep - 3.500000000000000000) < 1e-15)
        self.assertTrue(numpy.abs(eer - 0.279999999999999971) < 1e-15)

    def test_fast_actDCF(self):
        plo = logit(0.01)
        s1 = numpy.arange(len(y1))
        s2 = numpy.arange(len(y2))
        s3 = numpy.arange(len(y3))
        s4 = numpy.arange(len(y4))
        self.assertTrue(numpy.abs(fast_actDCF(s1[y1], s1[~y1], plo=plo, normalize=True) - 69.585714285714274752) < 1e-15)
        self.assertTrue(numpy.abs(fast_actDCF(s2[y2], s2[~y2], plo=plo, normalize=True) - 74.849999999999980105) < 1e-15)
        self.assertTrue(numpy.abs(fast_actDCF(s3[y3], s3[~y3], plo=plo, normalize=True) - 49.699999999999988631) < 1e-15)
        self.assertTrue(numpy.abs(fast_actDCF(s4[y4], s4[~y4], plo=plo, normalize=True) - 72.142857142857124586) < 1e-15)

    """
    def test_DETsort(self):
        self.fail()

    def test_compute_roc(self):
        self.fail()

    def test_filter_roc(self):
        self.fail()

    def test_plotseg(self):
        self.fail()

    def test_rocchdet(self):
        self.fail()
    """

    def test_cllr(self):
        s1 = numpy.arange(len(y1))
        s2 = numpy.arange(len(y2))
        s3 = numpy.arange(len(y3))
        s4 = numpy.arange(len(y4))
        self.assertTrue(numpy.abs(cllr(s1[y1], s1[~y1]) - 5.5021440402718857) < 1e-15)
        self.assertTrue(numpy.abs(cllr(s2[y2], s2[~y2]) - 5.3860729695565803) < 1e-15)
        self.assertTrue(numpy.abs(cllr(s3[y3], s3[~y3]) - 3.5383400165284744) < 1e-15)
        self.assertTrue(numpy.abs(cllr(s4[y4], s4[~y4]) - 6.8852296873376453) < 1e-15)

    def test_min_cllr(self):
        s1 = numpy.arange(len(y1))
        s2 = numpy.arange(len(y2))
        s3 = numpy.arange(len(y3))
        s4 = numpy.arange(len(y4))
        """
        now, here is something funny. The bosaris toolkit has minor errors in min cllr:
        // from MATLAB      => debugging values from this Python code (I know, supposedly not how one should test...)
        0.7553920738948063  => 0.7553920218552648
        1.0000000901685016  <-- especially this fellow is supposed to be == 1.0 but it's > 1.0  => 1.0
        0.6538470708991950  => 0.6538470444761644
        0.7885550580707099  => 0.7885550292638571
        """
        self.assertTrue(numpy.abs(min_cllr(s1[y1], s1[~y1], monotonicity_epsilon=1e-15) - 0.7553920218552648) < 1e-15)
        self.assertTrue(numpy.abs(min_cllr(s2[y2], s2[~y2], monotonicity_epsilon=1e-15) - 1.0) < 1e-15)
        self.assertTrue(numpy.abs(min_cllr(s3[y3], s3[~y3], monotonicity_epsilon=1e-15) - 0.6538470444761644) < 1e-15)
        self.assertTrue(numpy.abs(min_cllr(s4[y4], s4[~y4], monotonicity_epsilon=1e-15) - 0.7885550292638571) < 1e-15)
