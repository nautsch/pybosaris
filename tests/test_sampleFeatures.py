from unittest import TestCase
from pybosaris import SampleFeatures
import numpy
import copy
import os

__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"

testfile_txt = 'testfile_sf.txt'
testfile_h5 = 'testfile_sf.h5'
testfile_matlab = 'testfile_sf.hdf5'


class TestSampleFeatures(TestCase):
    def setUp(self):
        self.sf = SampleFeatures()
        self.sf.sample_ids = numpy.array(['a', 'b', 'c'], dtype='|O')
        self.sf.class_ids = numpy.array(['A', 'A', 'B'], dtype='|O')
        self.sf.features = numpy.random.rand(3)
        self.sf.start = numpy.empty(3, dtype='|O')
        self.sf.stop = numpy.empty(3, dtype='|O')
        assert (self.sf.validate())

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(testfile_txt):
            os.remove(testfile_txt)
        if os.path.exists(testfile_h5):
            os.remove(testfile_h5)
        if os.path.exists(testfile_matlab):
            os.remove(testfile_matlab)

    def test_write_read_h5(self):
        self.sf.write(testfile_h5)
        sf2 = SampleFeatures().read(testfile_h5)
        sf3 = SampleFeatures(filename=testfile_h5)
        self.assertEqual(self.sf, sf2)
        self.assertEqual(self.sf, sf3)

    def test_write_read_txt(self):
        self.sf.write_txt(testfile_txt)
        sf2 = SampleFeatures.read_txt(testfile_txt)
        self.assertEqual(self.sf, sf2)

    def test_validate(self):
        self.assertTrue(self.sf.validate())
        sf2 = SampleFeatures()
        sf2.sample_ids = self.sf.sample_ids
        sf2.class_ids = self.sf.class_ids
        sf2.features = self.sf.features
        sf2.start = self.sf.start
        sf2.stop = self.sf.stop
        self.assertTrue(sf2.validate())
        sf2.sample_ids = self.sf.sample_ids[1:]
        self.assertFalse(sf2.validate())
        sf2.sample_ids = self.sf.sample_ids
        sf2.class_ids = self.sf.class_ids[1:]
        self.assertFalse(sf2.validate())
        sf2.class_ids = self.sf.class_ids
        sf2.features = self.sf.features[1:]
        self.assertFalse(sf2.validate())
        sf2.features = self.sf.features
        sf2.start = self.sf.start[1:]
        self.assertFalse(sf2.validate())
        sf2.start = self.sf.start
        sf2.stop = self.sf.stop[1:]
        self.assertFalse(sf2.validate())
        sf2.stop = self.sf.stop
        sf2.features = numpy.concatenate((self.sf.features, numpy.random.rand(5)))
        self.assertFalse(sf2.validate())
        sf2.features = self.sf.features
        sf2.sample_ids = numpy.concatenate((['h', 'k'], self.sf.sample_ids))
        self.assertFalse(sf2.validate())

    def test_set(self):
        sample_ids = copy.deepcopy(self.sf.sample_ids)
        class_ids = copy.deepcopy(self.sf.class_ids)
        features = copy.deepcopy(self.sf.features)
        self.sf.set(sample_ids=sample_ids[:2], class_ids=class_ids[:2], features=features[:2])
        self.assertTrue((self.sf.sample_ids == sample_ids[:2]).all())
        self.assertTrue((self.sf.class_ids == class_ids[:2]).all())
        self.assertTrue((self.sf.features == features[:2]).all())
        self.sf.set(sample_ids=sample_ids, class_ids=class_ids, features=features)
        self.assertTrue((self.sf.sample_ids == sample_ids).all())
        self.assertTrue((self.sf.class_ids == class_ids).all())
        self.assertTrue((self.sf.features == features).all())

    def test_join(self):
        sf2 = copy.deepcopy(self.sf)
        sf2.features = numpy.random.rand(sf2.features.shape[0])
        assert (sf2.validate())

        sf3 = SampleFeatures.join(sf2, self.sf)
        self.assertEqual(sf3.class_ids.shape[0], sf2.sample_ids.shape[0])
        self.assertEqual(sf3.features.shape[0], sf2.sample_ids.shape[0])
        self.assertEqual(sf3.features.shape[1], 2)

    def test_merge(self):
        sf2 = SampleFeatures()
        sf2.sample_ids = numpy.array(['1', '2'])
        sf2.class_ids = numpy.array(['B', 'C'])
        sf2.features = numpy.random.rand(2)
        sf2.start = numpy.empty(2, dtype='|O')
        sf2.stop = numpy.empty(2, dtype='|O')
        assert (sf2.validate())

        sf3 = self.sf.merge(sf2)
        self.assertEqual(set(sf3.sample_ids), set(self.sf.sample_ids).union(set(sf2.sample_ids)))
        self.assertEqual(set(sf3.class_ids), set(self.sf.class_ids).union(set(sf2.class_ids)))
        self.assertEqual(set(sf3.features), set(self.sf.features).union(set(sf2.features)))
        self.assertEqual(set(sf3.start), set(self.sf.start).union(set(sf2.start)))
        self.assertEqual(set(sf3.stop), set(self.sf.stop).union(set(sf2.stop)))

    def test_align_with_ids(self):
        ids = self.sf.sample_ids[[0, 2, 1]]
        sf2 = self.sf.align_with_ids(ids)
        self.assertTrue((sf2.sample_ids == ids).all())
        self.assertFalse((self.sf.sample_ids == ids).all())
        scr3 = self.sf.align_with_ids(self.sf.sample_ids)
        self.assertTrue((scr3.sample_ids == self.sf.sample_ids).all())
        self.assertTrue((self.sf.sample_ids == scr3.sample_ids).all())
        self.assertTrue((self.sf.class_ids == scr3.class_ids).all())
        self.assertTrue((self.sf.features == scr3.features).all())
