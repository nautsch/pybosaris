from unittest import TestCase
from pybosaris import Key
import numpy
import copy
import os


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


testfile_txt = 'testfile_key.txt'
testfile_h5 = 'testfile_key.h5'
testfile_matlab = 'testfile_key.hdf5'


class TestKey(TestCase):
    def setUp(self):
        self.key = Key()
        self.key.modelset = numpy.array(['a', 'b', 'c'])
        self.key.segset = numpy.array(['X', 'Y', 'Z', 'U'])
        self.key.tar = numpy.array([[1, 1, 0,0], [0,0,1,0],[0,0,0,0]],dtype=bool)
        self.key.non = ~self.key.tar
        self.key.non[2,3] = False # unscored
        assert(self.key.validate())

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(testfile_txt):
            os.remove(testfile_txt)
        if os.path.exists(testfile_h5):
            os.remove(testfile_h5)
        if os.path.exists(testfile_matlab):
            os.remove(testfile_matlab)

    def test_write_read_h5(self):
        self.key.write(testfile_h5)
        key2 = Key.read(testfile_h5)
        key3 = Key(testfile_h5)
        self.assertEqual(self.key, key2)
        self.assertEqual(self.key, key3)

    def test_write_read_txt(self):
        self.key.write_txt(testfile_txt)
        key2 = Key.read_txt(testfile_txt)
        self.assertEqual(self.key, key2)

    def test_write_read_matlab(self):
        self.key.write_matlab(testfile_matlab)
        key2 = Key.read_matlab(testfile_matlab)
        self.assertEqual(self.key, key2)

    def test_filter(self):
        keep = False
        key2 = self.key.filter(modlist=self.key.modelset, seglist=self.key.segset, keep=keep)
        self.assertNotEqual(self.key, key2)
        self.assertTrue(key2.tar.size == 0)
        self.assertTrue(key2.non.size == 0)
        key3 = self.key.filter(modlist=self.key.modelset[1:], seglist=self.key.segset[2:], keep=keep)
        self.assertNotEqual(self.key, key3)
        self.assertTrue(key3.tar.size == 2)
        self.assertTrue(key3.non.size == 2)

        keep = True
        key4 = self.key.filter(modlist=self.key.modelset, seglist=self.key.segset, keep=keep)
        self.assertEqual(self.key, key4)
        key5 = self.key.filter(modlist=self.key.modelset[1:], seglist=self.key.segset[2:], keep=keep)
        self.assertNotEqual(self.key, key5)

    def test_to_ndx(self):
        ndx = self.key.to_ndx()
        self.assertTrue((ndx.modelset == self.key.modelset).all())
        self.assertTrue((ndx.modelset == self.key.modelset).all())
        self.assertTrue((ndx.trialmask == self.key.tar | self.key.non).all())

    def test_validate(self):
        self.assertTrue(self.key.validate())
        key2 = self.key
        key2.modelset = self.key.modelset[1:]
        self.assertFalse(key2.validate())
        key2.modelset = self.key.modelset
        key2.segset = self.key.segset[1:]
        self.assertFalse(key2.validate())
        key2.segset = self.key.segset
        key2.tar = self.key.tar[1:,:]
        self.assertFalse(key2.validate())
        key2.tar = self.key.tar
        key2.non = self.key.non[1:,:]
        self.assertFalse(key2.validate())

    def test_merge(self):
        key2 = Key()
        key2.modelset = numpy.array(['.',',',';'])
        key2.segset = numpy.array(['1','2'])
        key2.tar = numpy.random.rand(3,2) > 0.2
        key2.non = ~key2.tar
        assert(key2.validate())

        key3 = copy.deepcopy(key2)
        key3.merge([self.key])
        self.assertTrue(key3.validate())
        self.assertEqual(set(key3.modelset), set(self.key.modelset).union(set(key2.modelset)))
        self.assertEqual(set(key3.segset), set(self.key.segset).union(set(key2.segset)))
