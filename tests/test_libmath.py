from unittest import TestCase
from pybosaris.libmath import sigmoid, logit, probit, pavx, ismember, diff, optimal_llr
import numpy
from scipy.special import logit as scipy_logit
from scipy.stats import norm


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


# preparing pavx & optimal_llr tests
test_probabilities = numpy.random.rand(10)
test_odds = (numpy.random.rand(10) - 0.5) * 100
y1 = numpy.array([0,0,1,0,1,1,0,0,1,0,0,1,0,0,0,1,1], dtype=bool)
h1 = numpy.array([0, 5/13, 1])
w1 = numpy.array([2, 13, 2])
g1 = numpy.concatenate([numpy.repeat(h1[i], w1[i]) for i in range(h1.shape[0])])
y2 = numpy.array([1,0,1,1,0,0,1,0,0,1,0,0,0], dtype=bool)
h2 = numpy.array([5/13])
w2 = numpy.array([13])
g2 = numpy.concatenate([numpy.repeat(h2[i], w2[i]) for i in range(h2.shape[0])])
y3 = numpy.flipud(y2)
h3 = numpy.array([0, 1/3, 2/3, 1])
w3 = numpy.array([3, 6, 3, 1])
g3 = numpy.concatenate([numpy.repeat(h3[i], w3[i]) for i in range(h3.shape[0])])
y4 = numpy.array([1,1,0,0,0,0,1,0,0,0,1,0,1,1,1,0,1,1,1,0,1,0,1,1,1], dtype=bool)
h4 = numpy.array([0.3, 0.5, 0.7, 1])
w4 = numpy.array([10, 2, 10, 3])
g4 = numpy.concatenate([numpy.repeat(h4[i], w4[i]) for i in range(h4.shape[0])])


def helpers(y, h):
    db_prior = numpy.log(y.sum()/(1-y).sum())
    llr_raw_helper = logit(h) - db_prior
    return db_prior, llr_raw_helper


monotonicity_epsilon = 1e-16  # BOSARIS: 1e-6

# assumption: all scores (s=0:length(y)-1): s = numpy.arange(len(y))
db1_prior, llr_raw1_helper = helpers(y1, h1)
nw1 = [2, 8]
tw1 = [5, 2]
th1 = [llr_raw1_helper[1], llr_raw1_helper[2]]
nh1 = [llr_raw1_helper[0], llr_raw1_helper[1]]
tar_1_raw = numpy.concatenate([numpy.repeat(th1[i], tw1[i]) for i in range(len(th1))])
non_1_raw = numpy.concatenate([numpy.repeat(nh1[i], nw1[i]) for i in range(len(nh1))])
# Laplace's rule of succession
llr_raw1_helper[-1] = logit(3/4) - db1_prior  # idk why 3/4 = 9/12; y1.sum()=7; len(y1)=17
llr_raw1_helper[0]  = logit(1/4) - db1_prior  # idk why 1/4
th1 = [llr_raw1_helper[1], llr_raw1_helper[2]]
nh1 = [llr_raw1_helper[0], llr_raw1_helper[1]]
tar_1_laplace = numpy.concatenate([numpy.repeat(th1[i], tw1[i]) for i in range(len(th1))])
non_1_laplace = numpy.concatenate([numpy.repeat(nh1[i], nw1[i]) for i in range(len(nh1))])

db2_prior, llr_raw2_helper = helpers(y2, h2)           # special case: llr_raw2_helper = [0]
db2_helper = numpy.arange(1,len(y2)+1)/len(y2) * monotonicity_epsilon  # the monotonicity preserver remains as all LLRs = 0
tar_2_raw = db2_helper[numpy.where(y2)[0]]
non_2_raw = db2_helper[numpy.where(~y2)[0]]
# Laplace's rule of succession
tar_2_laplace = numpy.repeat(logit(6/15) - db2_prior, 5)  # idk why 6/15; y2.sum()=5; len(y2)=13
non_2_laplace = numpy.repeat(logit(6/15) - db2_prior, 8)

db3_prior, llr_raw3_helper = helpers(y3, h3)
tw3 = [2, 2, 1]
nw3 = [3, 4, 1]
th3 = [llr_raw3_helper[1], llr_raw3_helper[2], llr_raw3_helper[3]]
nh3 = [llr_raw3_helper[0], llr_raw3_helper[1], llr_raw3_helper[2]]
tar_3_raw = numpy.concatenate([numpy.repeat(th3[i], tw3[i]) for i in range(len(th3))])
non_3_raw = numpy.concatenate([numpy.repeat(nh3[i], nw3[i]) for i in range(len(nh3))])
# Laplace's rule of succession
llr_raw3_helper[-1] = logit(2/3) - db3_prior  # idk why 2/3 = 6/9 = 8/12 = 10/15; y3.sum()=5; len(y3)=13
llr_raw3_helper[0]  = logit(1/5) - db3_prior  # idk why 1/5 = 2/10 = 3/15
th3 = [llr_raw3_helper[1], llr_raw3_helper[2], llr_raw3_helper[3]]
nh3 = [llr_raw3_helper[0], llr_raw3_helper[1], llr_raw3_helper[2]]
tar_3_laplace = numpy.concatenate([numpy.repeat(th3[i], tw3[i]) for i in range(len(th3))])
non_3_laplace = numpy.concatenate([numpy.repeat(nh3[i], nw3[i]) for i in range(len(nh3))])

db4_prior, llr_raw4_helper = helpers(y4, h4)
tw4 = [3, 1, 7, 3]
nw4 = [7, 1, 3]
th4 = [llr_raw4_helper[0], llr_raw4_helper[1], llr_raw4_helper[2], llr_raw4_helper[3]]
nh4 = [llr_raw4_helper[0], llr_raw4_helper[1], llr_raw4_helper[2]]
tar_4_raw = numpy.concatenate([numpy.repeat(th4[i], tw4[i]) for i in range(len(th4))])
non_4_raw = numpy.concatenate([numpy.repeat(nh4[i], nw4[i]) for i in range(len(nh4))])
# Laplace's rule of succession
llr_raw4_helper[-1] = logit(4/5) - db4_prior  # idk why 4/5 = 16/20 = 20/25 = 24/30; y4.sum()=14; len(y4)=25
llr_raw4_helper[0]  = logit(1/3) - db4_prior
th4 = [llr_raw4_helper[0], llr_raw4_helper[1], llr_raw4_helper[2], llr_raw4_helper[3]]
nh4 = [llr_raw4_helper[0], llr_raw4_helper[1], llr_raw4_helper[2]]
tar_4_laplace = numpy.concatenate([numpy.repeat(th4[i], tw4[i]) for i in range(len(th4))])
non_4_laplace = numpy.concatenate([numpy.repeat(nh4[i], nw4[i]) for i in range(len(nh4))])


class TestScores(TestCase):
    def test_ismember(self):
        # ismember(list1, list2)
        list1 = ['a', 'b', 'c']
        list2 = ['a', 'b', 'c']
        list3 = ['a', 'c']
        list4 = ['d', 'e']
        self.assertEqual(ismember(list1, list2), [True, True, True])
        self.assertEqual(ismember(list1, list3), [True, False, True])
        self.assertEqual(ismember(list3, list1), [True, True])
        self.assertEqual(ismember(list1, list4), [False, False, False])
        self.assertEqual(ismember(list4, list1), [False, False])

    def test_diff(self):
        list1 = ['a', 'b', 'c']
        list2 = ['a', 'b', 'c']
        list3 = ['a', 'c']
        list4 = ['d', 'e']
        self.assertEqual(diff(list1, list2), [])
        self.assertEqual(diff(list1, list3), ['b'])
        self.assertEqual(diff(list1, list4), list1)
        self.assertEqual(diff(list4, list1), list4)

    def test_sigmoid(self):
        self.assertTrue((test_probabilities - sigmoid(logit(test_probabilities)) < 1e-15).all())

    def test_logit(self):
        self.assertTrue((logit(test_probabilities) == scipy_logit(test_probabilities)).all())

    def test_probit(self):
        self.assertTrue((test_probabilities - norm.cdf(probit(test_probabilities)) < 1e-15).all())

    def test_pavx(self):
        [rg1, rw1, rh1] = pavx(y1)
        self.assertTrue((rg1 - g1 < 1e-15).all())
        self.assertTrue((rw1 - w1 < 1e-15).all())
        self.assertTrue((rh1 - h1 < 1e-15).all())
        [rg2, rw2, rh2] = pavx(y2)
        self.assertTrue((rg2 - g2 < 1e-15).all())
        self.assertTrue((rw2 - w2 < 1e-15).all())
        self.assertTrue((rh2 - h2 < 1e-15).all())
        [rg3, rw3, rh3] = pavx(y3)
        self.assertTrue((rg3 - g3 < 1e-15).all())
        self.assertTrue((rw3 - w3 < 1e-15).all())
        self.assertTrue((rh3 - h3 < 1e-15).all())
        [rg4, rw4, rh4] = pavx(y4)
        self.assertTrue((rg4 - g4 < 1e-15).all())
        self.assertTrue((rw4 - w4 < 1e-15).all())
        self.assertTrue((rh4 - h4 < 1e-15).all())

    def test_optimal_llr(self):
        def nan_diff_equal(a, b):
            a_nan = a[numpy.isfinite(a)]
            b_nan = b[numpy.isfinite(b)]
            ok = (numpy.abs(a_nan - b_nan) < 1e-15).all()
            return ok
        
        s1 = numpy.arange(len(y1))
        s2 = numpy.arange(len(y2))
        s3 = numpy.arange(len(y3))
        s4 = numpy.arange(len(y4))
        t1,n1 = optimal_llr(s1[numpy.argwhere(y1).flatten()], s1[numpy.argwhere(~y1).flatten()], monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t1, tar_1_raw))
        self.assertTrue(nan_diff_equal(n1, non_1_raw))
        t2,n2 = optimal_llr(s2[numpy.argwhere(y2).flatten()], s2[numpy.argwhere(~y2).flatten()], monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t2, tar_2_raw))
        self.assertTrue(nan_diff_equal(n2, non_2_raw))
        t3,n3 = optimal_llr(s3[numpy.argwhere(y3).flatten()], s3[numpy.argwhere(~y3).flatten()], monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t3, tar_3_raw))
        self.assertTrue(nan_diff_equal(n3, non_3_raw))
        t4,n4 = optimal_llr(s4[numpy.argwhere(y4).flatten()], s4[numpy.argwhere(~y4).flatten()], monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t4, tar_4_raw))
        self.assertTrue(nan_diff_equal(n4, non_4_raw))
        t1l,n1l = optimal_llr(s1[numpy.argwhere(y1).flatten()], s1[numpy.argwhere(~y1).flatten()], laplace=True, monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t1l, tar_1_laplace))
        self.assertTrue(nan_diff_equal(n1l, non_1_laplace))
        t2l,n2l = optimal_llr(s2[numpy.argwhere(y2).flatten()], s2[numpy.argwhere(~y2).flatten()], laplace=True, monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t2l, tar_2_laplace))
        self.assertTrue(nan_diff_equal(n2l, non_2_laplace))
        t3l,n3l = optimal_llr(s3[numpy.argwhere(y3).flatten()], s3[numpy.argwhere(~y3).flatten()], laplace=True, monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t3l, tar_3_laplace))
        self.assertTrue(nan_diff_equal(n3l, non_3_laplace))
        t4l,n4l = optimal_llr(s4[numpy.argwhere(y4).flatten()], s4[numpy.argwhere(~y4).flatten()], laplace=True, monotonicity_epsilon=monotonicity_epsilon)
        self.assertTrue(nan_diff_equal(t4l, tar_4_laplace))
        self.assertTrue(nan_diff_equal(n4l, non_4_laplace))
