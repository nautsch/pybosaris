from unittest import TestCase
from pybosaris import Scores, Key
import numpy
import copy
import os


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


testfile_txt = 'testfile_scr.txt'
testfile_h5 = 'testfile_scr.h5'
testfile_matlab = 'testfile_scr.hdf5'

key = Key()
key.modelset = numpy.array(['a', 'b', 'c'])
key.segset = numpy.array(['X', 'Y', 'Z', 'U'])
key.tar = numpy.array([[1, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]], dtype=bool)
key.non = ~key.tar
key.non[2, 3] = False  # unscored
assert(key.validate())


class TestScores(TestCase):
    def setUp(self):
        self.scr = Scores()
        self.scr.modelset = numpy.array(['a', 'b', 'c'])
        self.scr.segset = numpy.array(['X', 'Y', 'Z', 'U'])
        self.scr.scoremask = numpy.array([[1, 1, 0,1], [1,1,1,1],[1,1,1,1]],dtype=bool)
        self.scr.scoremat = numpy.random.rand(3, 4)
        assert(self.scr.validate())

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(testfile_txt):
            os.remove(testfile_txt)
        if os.path.exists(testfile_h5):
            os.remove(testfile_h5)
        if os.path.exists(testfile_matlab):
            os.remove(testfile_matlab)
    
    def test_write_read_h5(self):
        self.scr.write(testfile_h5)
        scr2 = Scores.read(testfile_h5)
        scr3 = Scores(testfile_h5)
        self.assertEqual(self.scr, scr2)
        self.assertEqual(self.scr, scr3)

    def test_write_read_txt(self):
        self.scr.write_txt(testfile_txt)
        scr2 = Scores.read_txt(testfile_txt)
        self.assertEqual(self.scr, scr2)

    def test_write_read_matlab(self):
        self.scr.write_matlab(testfile_matlab)
        scr2 = Scores.read_matlab(testfile_matlab)
        self.assertEqual(self.scr, scr2)

    def test_get_tar_non(self):
        [t,n] = self.scr.get_tar_non(key)
        self.assertTrue((t == self.scr.scoremat[key.tar & self.scr.scoremask].flatten()).all())
        self.assertTrue((n == self.scr.scoremat[key.non & self.scr.scoremask].flatten()).all())
        self.assertNotEqual(n.shape, self.scr.scoremat[key.non].flatten().shape)
        # note: the last line holds in the chosen example for tar scores

    def test_align_with_ndx(self):
        ndx = key.to_ndx()
        ndx.modelset = ndx.modelset[[1,2,0]]
        ndx.trialmask = ndx.trialmask[[1,2,0], :]
        scr2 = self.scr.align_with_ndx(ndx)
        self.assertTrue((scr2.modelset == ndx.modelset).all())
        self.assertFalse((self.scr.modelset == ndx.modelset).all())
        scr3 = self.scr.align_with_ndx(key)
        self.assertTrue((scr3.modelset == key.modelset).all())
        self.assertTrue((self.scr.modelset == scr3.modelset).all())
        self.assertTrue((self.scr.scoremat == scr3.scoremat).all())

    def test_set_missing_to_value(self):
        ndx = key.to_ndx()
        ndx.trialmask = numpy.ones(self.scr.scoremask.shape, dtype=bool)
        idx_missing = ndx.trialmask & ~self.scr.scoremask
        scr2 = self.scr.set_missing_to_value(ndx=ndx, value=13)
        self.assertEqual(scr2.scoremat[idx_missing], 13)

    def test_filter(self):
        keep = False
        scr2 = self.scr.filter(modlist=self.scr.modelset, seglist=self.scr.segset, keep=keep)
        self.assertNotEqual(self.scr, scr2)
        self.assertTrue(scr2.scoremat.size == 0)
        self.assertTrue(scr2.scoremask.size == 0)
        scr3 = self.scr.filter(modlist=self.scr.modelset[1:], seglist=self.scr.segset[2:], keep=keep)
        self.assertNotEqual(self.scr, scr3)
        self.assertTrue(scr3.scoremat.size == 2)
        self.assertTrue(scr3.scoremask.size == 2)

        keep = True
        scr4 = self.scr.filter(modlist=self.scr.modelset, seglist=self.scr.segset, keep=keep)
        self.assertEqual(self.scr, scr4)
        scr5 = self.scr.filter(modlist=self.scr.modelset[1:], seglist=self.scr.segset[2:], keep=keep)
        self.assertNotEqual(self.scr, scr5)

    def test_validate(self):
        self.assertTrue(self.scr.validate())
        scr2 = self.scr
        scr2.modelset = self.scr.modelset[1:]
        self.assertFalse(scr2.validate())
        scr2.modelset = self.scr.modelset
        scr2.segset = self.scr.segset[1:]
        self.assertFalse(scr2.validate())
        scr2.segset = self.scr.segset
        scr2.scoremat = self.scr.scoremat[1:,:]
        self.assertFalse(scr2.validate())
        scr2.scoremat = self.scr.scoremat
        scr2.scoremask = self.scr.scoremask[1:,:]
        self.assertFalse(scr2.validate())

    def test_merge(self):
        scr2 = Scores()
        scr2.modelset = numpy.array(['.',',',';'])
        scr2.segset = numpy.array(['1','2'])
        scr2.scoremask = numpy.random.rand(3,2) > 0.2
        scr2.scoremat = numpy.random.rand(3,2)
        assert(scr2.validate())

        scr3 = copy.deepcopy(scr2)
        scr3.merge([self.scr])
        self.assertTrue(scr3.validate())
        self.assertEqual(set(scr3.modelset), set(self.scr.modelset).union(set(scr2.modelset)))
        self.assertEqual(set(scr3.segset), set(self.scr.segset).union(set(scr2.segset)))

    def test_sort(self):
        scr2 = copy.deepcopy(self.scr)
        scr2.modelset = scr2.modelset[[1,2,0]]
        scr2.scoremask = scr2.scoremask[[1,2,0], :]
        scr2.scoremat = scr2.scoremat[[1,2,0], :]
        scr3 = copy.deepcopy(scr2)
        scr3.sort()
        self.assertFalse((scr2.modelset == scr3.modelset).all())
        self.assertListEqual(list(self.scr.modelset), list(scr3.modelset))
        self.assertEqual(self.scr, scr3)

    def test_get_score(self):
        s1 = self.scr.get_score(modelID=self.scr.modelset[1], segID=self.scr.segset[3])
        s2 = self.scr.get_score(modelID=self.scr.modelset[0], segID=self.scr.segset[1])
        s3 = self.scr.get_score(modelID=self.scr.modelset[2], segID=self.scr.segset[0])
        self.assertEqual(s1, self.scr.scoremat[1,3])
        self.assertEqual(s2, self.scr.scoremat[0,1])
        self.assertEqual(s3, self.scr.scoremat[2,0])
