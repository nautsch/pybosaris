from unittest import TestCase
from pybosaris import SampleQuality
import numpy
import copy
import os


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


testfile_txt = 'testfile_sq.txt'
testfile_h5 = 'testfile_sq.h5'
testfile_matlab = 'testfile_sq.hdf5'


class TestSampleQuality(TestCase):
    def setUp(self):
        self.sq = SampleQuality()
        self.sq.sample_ids = numpy.array(['a', 'b', 'c'], dtype='|O')
        self.sq.quality = numpy.random.rand(3)
        self.sq.start = numpy.empty(3, dtype='|O')
        self.sq.stop = numpy.empty(3, dtype='|O')
        assert(self.sq.validate())

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(testfile_txt):
            os.remove(testfile_txt)
        if os.path.exists(testfile_h5):
            os.remove(testfile_h5)
        if os.path.exists(testfile_matlab):
            os.remove(testfile_matlab)
    
    def test_write_read_h5(self):
        self.sq.write(testfile_h5)
        sq2 = SampleQuality().read(testfile_h5)
        sq3 = SampleQuality(filename=testfile_h5)
        self.assertEqual(self.sq,sq2)
        self.assertEqual(self.sq,sq3)

    def test_write_read_txt(self):
        self.sq.write_txt(testfile_txt)
        sq2 = SampleQuality.read_txt(testfile_txt)
        self.assertEqual(self.sq, sq2)

    def test_validate(self):
        self.assertTrue(self.sq.validate())
        sq2 = SampleQuality()
        sq2.sample_ids = self.sq.sample_ids
        sq2.quality = self.sq.quality
        sq2.start = self.sq.start
        sq2.stop = self.sq.stop
        self.assertTrue(sq2.validate())
        sq2.sample_ids = self.sq.sample_ids[1:]
        self.assertFalse(sq2.validate())
        sq2.sample_ids = self.sq.sample_ids
        sq2.quality = self.sq.quality[1:]
        self.assertFalse(sq2.validate())
        sq2.quality = self.sq.quality
        sq2.start = self.sq.start[1:]
        self.assertFalse(sq2.validate())
        sq2.start = self.sq.start
        sq2.stop = self.sq.stop[1:]
        self.assertFalse(sq2.validate())
        sq2.stop = self.sq.stop
        sq2.quality = numpy.concatenate((self.sq.quality,numpy.random.rand(5)))
        self.assertFalse(sq2.validate())
        sq2.quality = self.sq.quality
        sq2.sample_ids = numpy.concatenate((['h','k'],self.sq.sample_ids))
        self.assertFalse(sq2.validate())

    def test_set(self):
        sample_ids = copy.deepcopy(self.sq.sample_ids)
        quality = copy.deepcopy(self.sq.quality)
        self.sq.set(sample_ids=sample_ids[:2],quality=quality[:2])
        self.assertTrue((self.sq.sample_ids == sample_ids[:2]).all())
        self.assertTrue((self.sq.quality == quality[:2]).all())
        self.sq.set(sample_ids=sample_ids,quality=quality)
        self.assertTrue((self.sq.sample_ids == sample_ids).all())
        self.assertTrue((self.sq.quality == quality).all())

    def test_join(self):
        sq2 = copy.deepcopy(self.sq)
        sq2.quality = numpy.random.rand(sq2.quality.shape[0])
        assert(sq2.validate())

        sq3 = SampleQuality.join(sq2, self.sq)
        self.assertEqual(sq3.quality.shape[0], sq2.sample_ids.shape[0])
        self.assertEqual(sq3.quality.shape[1], 2)

    def test_merge(self):
        sq2 = SampleQuality()
        sq2.sample_ids = numpy.array(['1', '2'])
        sq2.quality = numpy.random.rand(2)
        sq2.start = numpy.empty(2,dtype='|O')
        sq2.stop = numpy.empty(2,dtype='|O')
        assert(sq2.validate())

        sq3 = self.sq.merge(sq2)
        self.assertEqual(set(sq3.sample_ids), set(self.sq.sample_ids).union(set(sq2.sample_ids)))
        self.assertEqual(set(sq3.quality), set(self.sq.quality).union(set(sq2.quality)))
        self.assertEqual(set(sq3.start), set(self.sq.start).union(set(sq2.start)))
        self.assertEqual(set(sq3.stop), set(self.sq.stop).union(set(sq2.stop)))

    def test_align_with_ids(self):
        ids = self.sq.sample_ids[[0,2,1]]
        sq2 = self.sq.align_with_ids(ids)
        self.assertTrue((sq2.sample_ids == ids).all())
        self.assertFalse((self.sq.sample_ids == ids).all())
        scr3 = self.sq.align_with_ids(self.sq.sample_ids)
        self.assertTrue((scr3.sample_ids == self.sq.sample_ids).all())
        self.assertTrue((self.sq.sample_ids == scr3.sample_ids).all())
        self.assertTrue((self.sq.quality == scr3.quality).all())
