from unittest import TestCase
from pybosaris import Ndx
import numpy
import copy
import os


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


testfile_txt = 'testfile_ndx.txt'
testfile_h5 = 'testfile_ndx.h5'
testfile_matlab = 'testfile_ndx.hdf5'


class TestNdx(TestCase):
    def setUp(self):
        self.ndx = Ndx()
        self.ndx.modelset = numpy.array(['a', 'b', 'c'])
        self.ndx.segset = numpy.array(['X', 'Y', 'Z', 'U'])
        self.ndx.trialmask = numpy.array([[1, 1, 0,1], [1,1,1,1],[1,1,1,1]],dtype=bool)
        """
        # note: these lines will make test_write_read_txt fail
        self.ndx.trialmask[2, :] = False
        self.ndx.trialmask[:, 2] = False
        # but these don't matter, as the 'trialmask aligned' ndx derivatives would be equal... 
        """
        assert(self.ndx.validate())

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(testfile_txt):
            os.remove(testfile_txt)
        if os.path.exists(testfile_h5):
            os.remove(testfile_h5)
        if os.path.exists(testfile_matlab):
            os.remove(testfile_matlab)

    def test_write_read_h5(self):
        self.ndx.write(testfile_h5)
        ndx2 = Ndx.read(testfile_h5)
        ndx3 = Ndx(testfile_h5)
        self.assertEqual(self.ndx, ndx2)
        self.assertEqual(self.ndx, ndx3)

    def test_write_read_txt(self):
        self.ndx.write_txt(testfile_txt)
        ndx2 = Ndx.read_txt(testfile_txt)
        self.assertEqual(self.ndx, ndx2)

    def test_filter(self):
        keep = False
        ndx2 = self.ndx.filter(modlist=self.ndx.modelset, seglist=self.ndx.segset, keep=keep)
        self.assertNotEqual(self.ndx, ndx2)
        self.assertTrue(ndx2.trialmask.size == 0)
        ndx3 = self.ndx.filter(modlist=self.ndx.modelset[1:], seglist=self.ndx.segset[2:], keep=keep)
        self.assertNotEqual(self.ndx, ndx3)
        self.assertTrue(ndx3.trialmask.size == 2)

        keep = True
        ndx4 = self.ndx.filter(modlist=self.ndx.modelset, seglist=self.ndx.segset, keep=keep)
        self.assertEqual(self.ndx, ndx4)
        ndx5 = self.ndx.filter(modlist=self.ndx.modelset[1:], seglist=self.ndx.segset[2:], keep=keep)
        self.assertNotEqual(self.ndx, ndx5)

    def test_validate(self):
        self.assertTrue(self.ndx.validate())
        ndx2 = self.ndx
        ndx2.modelset = self.ndx.modelset[1:]
        self.assertFalse(ndx2.validate())
        ndx2.modelset = self.ndx.modelset
        ndx2.segset = self.ndx.segset[1:]
        self.assertFalse(ndx2.validate())
        ndx2.segset = self.ndx.segset
        ndx2.trialmask = self.ndx.trialmask[1:,:]
        self.assertFalse(ndx2.validate())

    def test_merge(self):
        ndx2 = Ndx()
        ndx2.modelset = numpy.array(['.',',',';'])
        ndx2.segset = numpy.array(['1','2'])
        ndx2.trialmask = numpy.random.rand(3,2) > 0.2
        assert(ndx2.validate())

        ndx3 = copy.deepcopy(ndx2)
        ndx3.merge([self.ndx])
        self.assertTrue(ndx3.validate())
        self.assertEqual(set(ndx3.modelset), set(self.ndx.modelset).union(set(ndx2.modelset)))
        self.assertEqual(set(ndx3.segset), set(self.ndx.segset).union(set(ndx2.segset)))
