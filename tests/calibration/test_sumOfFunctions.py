from unittest import TestCase
from pybosaris.calibration.objectives import SumOfFunctions, GeneralMatrixMultiplication, Transpose
from tests.calibration.test_objectiveFunction import test_MV2DF
import numpy


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


A = numpy.random.rand(4, 4)
B = numpy.random.rand(4, 4)
W = numpy.concatenate((A.flatten(), B.flatten()))

weights = numpy.array([-1, 1])


class TestSumOfFunctions(TestCase):
    def setUp(self):
        # f = GeneralMatrixMultiplication(m=4, k=4, n=4);
        # g = transpose_mv2df(f,4,4)
        # for f = A*B;
        # for g = B'*A';
        # s = sum_of_functions(w,[-1,1],f,g);
        # with s = stack(w,f,g);
        self.f = SumOfFunctions(
            weights=weights,
            f=GeneralMatrixMultiplication(m=4, k=4, n=4),
            g=Transpose(m=4, n=4).compose_with(GeneralMatrixMultiplication(m=4, k=4, n=4))
        )

    def test_BOSARIS(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)
