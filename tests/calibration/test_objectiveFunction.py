from unittest import TestCase
from pybosaris.calibration.objectives import ObjectiveFunction, GeneralMatrixMultiplication, Transpose
import numpy
import logging
from functools import partial
import copy


logging.basicConfig(format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"


class DummyObjective(ObjectiveFunction):

    def __init__(self):
        super().__init__()

    def objective(self, w):
        return w

    def objective_and_gradient(self, w):
        return w, numpy.gradient

    def objective_and_derivatives(self, w):
        def derivs():
            return numpy.gradient, self.hessian, False

        return w, derivs

    # https://stackoverflow.com/questions/31206443/numpy-second-derivative-of-a-ndimensional-array
    def hessian(self, x):
        x_grad = numpy.gradient(x)
        hessian = numpy.empty((x.ndim, x.ndim) + x.shape, dtype=x.dtype)
        for k, grad_k in enumerate(x_grad):
            tmp_grad = numpy.gradient(grad_k)
            for l, grad_kl in enumerate(tmp_grad):
                hessian[k, l, :, :] = grad_kl
        return hessian

    def hessian_and_jacobian_product(self, x):
        return self.hessian(x), None


def test_MV2DF(f, x0, do_cstep=True):
    assert(isinstance(f, ObjectiveFunction))

    # x0 = x0.flatten()

    Jc = 0
    if do_cstep:
        Jc = cstepJacobian(f, x0)

    Jr = rstepJacobian(f, x0)

    y0, deriv = f.objective_and_derivatives(x0)
    _, gradient = f.objective_and_gradient(x0)
    n = len(x0)
    if numpy.isscalar(y0):
        m = 1
    else:
        m = len(y0)

    J2 = numpy.zeros(Jr.shape)

    for i in numpy.arange(m):
        y = numpy.zeros(m)
        y[i] = 1
        if numpy.isscalar(y0):
            J2[:] = gradient(y)
        else:
            J2[i, :] = gradient(y)

    if do_cstep:
        c_err = numpy.max(numpy.abs(Jc - J2))
    else:
        c_err = numpy.nan

    r_err = numpy.max(numpy.abs(Jr - J2))
    test_gradient_cstep_err = copy.deepcopy(c_err)
    test_gradient_rstep_err = copy.deepcopy(r_err)
    logging.info('test gradient : cstep err = {}, rstep err = {}'.format(c_err, r_err))

    g2 = numpy.random.rand(m)
    dummy, hess, linear, hess_jacob = deriv(g2)

    if True:  # ~linear
        rHess = partial(rstep_approxHess, dy=g2, f=f, x0=x0)
        if do_cstep:
            cHess = partial(cstep_approxHess, dy=g2, f=f, x0=x0)
        else:
            cHess = lambda dx: numpy.full(dx.shape, numpy.nan)

    J1 = numpy.zeros(Jr.shape)
    if True:  # ~linear
        H1 = numpy.zeros((n, n))
        H2 = numpy.zeros((n, n))
        Hr = numpy.zeros((n, n))
        Hc = numpy.zeros((n, n))

    for j in numpy.arange(n):
        x = numpy.zeros(n)
        x[j] = 1
        h1, jx = hess_jacob(x)
        h2 = hess(x)

        if numpy.isscalar(y0):
            J1[j] = jx
        else:
            J1[:, j] = jx

        if not linear:
            H1[:, j] = h1
            H2[:, j] = h2

        Hr[:, j] = rHess(x)
        Hc[:, j] = cHess(x)

    if do_cstep:
        c_err = numpy.max(numpy.abs(Jc - J1))
    else:
        c_err = numpy.nan

    r_err = numpy.max(numpy.abs(Jr - J1))
    test_jacobian_cstep_err = copy.deepcopy(c_err)
    test_jacobian_rstep_err = copy.deepcopy(r_err)
    logging.info('test Jacobian : cstep err = {}, rstep err = {}'.format(c_err, r_err))

    test_diff_gradient_jacobian_err = copy.deepcopy(numpy.max(numpy.abs(J1-J2)))
    logging.info('test Jacobian-gradient'': {}'.format(test_diff_gradient_jacobian_err))

    """
    if False:  # linear
        logging.log('function claims to be linear, not testing Hessians')
        return
    """

    r_err = numpy.max(numpy.abs(H1 - Hr))
    c_err = numpy.max(numpy.abs(H1 - Hc))
    rc_err = numpy.max(numpy.abs(Hr - Hc))

    test_hessian_cstep_err = copy.deepcopy(c_err)
    test_hessian_rstep_err = copy.deepcopy(r_err)
    test_hessian_rcstep_err = copy.deepcopy(rc_err)
    logging.info('test Hess prod: cstep err = {}, rstep err = {}, cstep-rstep = {}'.format(c_err, r_err, rc_err))

    test_diff_hessian_jacobian_err = copy.deepcopy(numpy.max(numpy.abs(H1-H2)))
    logging.info('test H1-H2: {}'.format(numpy.max(numpy.abs(H1 - H2))))
    return test_gradient_cstep_err, test_gradient_rstep_err, \
           test_jacobian_cstep_err, test_jacobian_rstep_err, \
           test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
           test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err


def rstep_approxHess(dx, dy, f, x0):
    assert(isinstance(f, ObjectiveFunction))
    alpha = numpy.sqrt(numpy.finfo(x0.dtype).eps)
    x2 = x0 + alpha * dx
    dummy, deriv2 = f.objective_and_gradient(x2)
    x1 = x0 - alpha * dx
    dummy, deriv1 = f.objective_and_gradient(x1)
    g2 = deriv2(dy)
    g1 = deriv1(dy)
    x = (g2 - g1) / (2 * alpha)
    return x


def cstep_approxHess(dx, dy, f, x0):
    assert(isinstance(f, ObjectiveFunction))
    x = x0 + 1e-20j * dx
    dummy, deriv = f.objective_and_gradient(x)
    g = deriv(dy)
    p = 1e20 * numpy.imag(g)
    return p


def cstepJacobian(f, w):
    assert(isinstance(f, ObjectiveFunction))
    x0 = w
    y0 = f.objective(x0)
    n = len(x0)
    if numpy.isscalar(y0):
        J = numpy.zeros(n)
    else:
        m = len(y0)
        J = numpy.zeros((m, n))

    for i in numpy.arange(n):
        x = x0.astype(numpy.complex)
        x[i] = x0[i] + 1e-20j
        y = f.objective(x)
        if numpy.isscalar(y0):
            J[i] = 1e20 * numpy.imag(y)
        else:
            J[:, i] = 1e20 * numpy.imag(y)
    return J


def rstepJacobian(f, w):
    assert(isinstance(f, ObjectiveFunction))
    x0 = w
    y0 = f.objective(x0)
    n = len(x0)
    if numpy.isscalar(y0):
        J = numpy.zeros(n)
    else:
        m = len(y0)
        J = numpy.zeros((m, n))

    dx = numpy.sqrt(numpy.finfo(w.dtype).eps)
    for i in numpy.arange(n):
        x = copy.deepcopy(x0)
        x[i] = x0[i] + dx
        y1 = f.objective(x)

        x = copy.deepcopy(x0)
        x[i] = x0[i] - dx
        y0 = f.objective(x)
        if numpy.isscalar(y0):
            J[i] = (y1 - y0) / (2 * dx)
        else:
            J[:, i] = (y1 - y0) / (2 * dx)

    return J
