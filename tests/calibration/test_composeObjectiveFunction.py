from unittest import TestCase
from pybosaris.calibration.objectives import GeneralMatrixMultiplication, LinearTransform
from tests.calibration.test_objectiveFunction import test_MV2DF
import numpy


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"
__credits__ = ["Niko Brummer", "Edward de Villiers"]


A = numpy.random.rand(4, 5)
B = numpy.random.rand(5, 4)
W = numpy.concatenate((A.flatten(), B.flatten()))
T1 = numpy.random.rand(40, 40)
T2 = numpy.random.rand(16, 16)
w2 = numpy.random.rand(10, 1)
T3 = numpy.random.rand(11, 10)
T4 = numpy.random.rand(5, 11)
W2 = numpy.random.rand(10)


class TestComposeObjectiveFunction(TestCase):
    def setUp(self):
        # f = @(w) gemm(w, 4, 5, 4)
        # g1 = gemm(f, 2, 4, 2) --> f1
        self.f1 = GeneralMatrixMultiplication(m=2, k=4, n=2).compose_with(
            inner=GeneralMatrixMultiplication(m=4, k=5, n=4))

        # f = @(w) linTrans(w, @ (x) T1 * x, @(y) T1.'*y)
        # g2 = gemm(f, 4, 5, 4) --> f2
        self.f2 = GeneralMatrixMultiplication(m=4, k=5, n=4).compose_with(
            inner=LinearTransform(map=lambda x: T1 @ x, transmap=lambda y: T1.T @ y))

        # f = @(w) gemm(w, 4, 5, 4)
        # g3 = linTrans(f, @ (x) T2 * x, @(y) T2.'*y) --> f3
        self.f3 = LinearTransform(map=lambda x: T2 @ x, transmap=lambda y: T2.T @ y).compose_with(
            inner=GeneralMatrixMultiplication(m=4, k=5, n=4))

        # f = @(w) linTrans(w2, @ (x) T3 * x, @(y) T3.'*y)
        # g4 = linTrans(f, @ (x) T4 * x, @(y) T4.'*y) --> f4
        self.f4 = LinearTransform(map=lambda x: T4 @ x, transmap=lambda y: T4.T @ y).compose_with(
            inner=LinearTransform(map=lambda x: T3 @ x, transmap=lambda y: T3.T @ y))


    def test_BOSARIS_non_linear_non_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f1, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        # TODO idk why the deltas are worse than for the others
        self.assertTrue(test_gradient_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err < 1e-14)  # == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

    def test_BOSARIS_linear_non_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f2, W)
        # TODO idk why the deltas are worse than for the others
        self.assertTrue(test_gradient_cstep_err < 1e-12)  # 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-12)  # 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-12)  # 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-4)  # 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-4)  # 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-4)  # 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-4)  # 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err < 1e-12)  # == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

    def test_BOSARIS_non_linear_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f3, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        # TODO idk why the deltas are worse than for the others
        self.assertTrue(test_gradient_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err < 1e-6)  # == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

    def test_BOSARIS_linear_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f4, W2)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        # TODO idk why the deltas are worse than for the others
        self.assertTrue(test_gradient_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err < 1e-6)  # == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)
