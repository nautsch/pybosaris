from unittest import TestCase
from pybosaris.calibration.objectives import evaluate_objective
from pybosaris.libperformance import cllr
import numpy


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"
__credits__ = ["Niko Brummer", "Edward de Villiers"]


class TestEvaluate(TestCase):
    def test_evaluate(self):
        num_trials = 20
        scores = numpy.random.normal(loc=0, scale=1, size=num_trials)
        classf = numpy.hstack((numpy.ones(int(num_trials/2)),-numpy.ones(int(num_trials/2))))
        prior = 0.5
        res = evaluate_objective(scores=scores, classf=classf, prior=prior, objective_function=None)  # None: Cllr
        cllr_val = cllr(tar_llrs=scores[classf > 0], nontar_llrs=scores[classf < 0])
        self.assertTrue(numpy.abs(res - cllr_val) < 1e-14)
