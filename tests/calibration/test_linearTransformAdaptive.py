from unittest import TestCase
from pybosaris.calibration.objectives import LinearTransformAdaptive
from tests.calibration.test_objectiveFunction import test_MV2DF
import numpy


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"
__credits__ = ["Niko Brummer", "Edward de Villiers"]


A = numpy.random.rand(4, 5)
first = 2 - 1
len = 3
map = lambda w: w[first:first+len]
W = numpy.random.rand(5)


def transmap(y, wlen):
    w = numpy.zeros(wlen)
    w[first:first+len] = y
    return w


class TestLinearTransformAdaptive(TestCase):
    def setUp(self):
        self.f = LinearTransformAdaptive(map=map, transmap=transmap)

    def test_BOSARIS(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)
