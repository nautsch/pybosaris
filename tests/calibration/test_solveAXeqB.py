from unittest import TestCase
from pybosaris.calibration.objectives import SolveAXeqB
from tests.calibration.test_objectiveFunction import test_MV2DF
import numpy


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"
__credits__ = ["Niko Brummer", "Edward de Villiers"]


A = numpy.random.rand(5, 5)
B = numpy.random.rand(5)
W = numpy.concatenate((A.flatten(), B.flatten()))  # or A.T.flatten() ?


class TestSolveAXeqB(TestCase):
    def setUp(self):
        self.f = SolveAXeqB(m=5)

    def test_BOSARIS(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f, W)
        self.assertTrue(test_gradient_cstep_err < 1e-10)  # 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-10)  # 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-10)  # 1e-14)
        # TODO idk why the deltas are worse than for the others, I think it is because of subsequent roundings befor linalg.solve
        self.assertTrue(test_gradient_rstep_err < 1e-5)  # 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-5)  # 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-5)  # 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-5)  # 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err < 1e-10)  # == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

        # TODO 2019-05-03 16:55:31.186 INFO     test Hess prod: cstep err = 7.962808012962341e-08, rstep err = 0.006142390659078956, cstep-rstep = 0.0061423665611073375
        # self.fail()
