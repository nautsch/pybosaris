from unittest import TestCase
from pybosaris.calibration.linear_fuser import LinearFuser
from pybosaris.calibration.objectives import evaluate_objective
from pybosaris.calibration.training import train_binary_classifier
from pybosaris.libperformance import cllr, min_cllr
from tests.calibration.test_objectiveFunction import test_MV2DF
from tests.test_libmath import y1, y2, y3, y4
import numpy
import logging


logging.basicConfig(format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"
__credits__ = ["Niko Brummer", "Edward de Villiers"]


N = 100
no_of_systems = 2


def make_data(N, means):
    dim, K = means.shape
    X = 5 * numpy.random.normal(loc=0, scale=1, size=dim * K * N).reshape(dim, K * N)  # noise
    ii = numpy.arange(N)
    for k in numpy.arange(K):
        X[:, ii] = means[:, k][:, None] + X[:, ii]
        ii = ii + N

    N = K * N
    tar = X[:, numpy.arange(int(N / 2))]
    non = X[:, numpy.arange(start=int(N / 2), stop=N)]
    return tar, non


class TestLinearFuser(TestCase):
    def test_splitvec_fh(self):
        head, tail = LinearFuser.splitvec_fh(head_size=2)
        W = numpy.asarray([1, 2, 3, 4, 5], dtype=numpy.float)

        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(head, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(tail, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

    def test_BOSARIS(self):
        # ----------------synthesize training data -------------------
        # randn('state',0);
        means = numpy.random.normal(loc=0, scale=1, size=no_of_systems * 2).reshape(no_of_systems, 2) * 8  # signal
        tar, non = make_data(N, means)

        # ------------- create system ------------------------------

        train_scores = numpy.vstack((tar.T, non.T)).T
        fuser = LinearFuser(scores=train_scores)

        # ------------- train it ------------------------------

        ntar = tar.shape[1]
        nnon = non.shape[1]
        classf = numpy.hstack((numpy.ones(ntar), -numpy.ones(nnon)))

        prior = 0.1
        maxiters = 50
        quiet = True
        objfun = None
        w0 = fuser.w
        w, train_cxe, w_pen, optimizerState, converged = train_binary_classifier(
            classifier=fuser, classf=classf, w0=w0,
            objective_function=objfun, prior=prior,
            penalizer=None, penalizer_weight=0,
            maxiters=maxiters, maxCG=100, optimizerState=None, quiet=quiet, cstepHessian=True)
        train_fuser = LinearFuser(scores=train_scores, w=w)
        train_fused_scores = train_fuser.fusion()
        train_c = cllr(train_fused_scores[:ntar], train_fused_scores[ntar:])
        train_c_min = min_cllr(train_fused_scores[:ntar], train_fused_scores[ntar:])
        logging.info('train Cxe = {}, cllr: {}, min: {}'.format(train_cxe, train_c, train_c_min))

        # ------------- test it ------------------------------

        tar, non = make_data(N, means)

        scores = numpy.vstack((tar.T, non.T)).T
        tail = numpy.asarray([1, 2, 3])
        wbig = numpy.hstack((w, tail))
        lin_fuser = LinearFuser(w=wbig, scores=scores)
        self.assertTrue((tail == lin_fuser.tail).all())
        fused_scores = lin_fuser.fusion()
        test_cxe = evaluate_objective(scores=fused_scores, classf=classf, prior=prior)
        c = cllr(fused_scores[:ntar], fused_scores[ntar:])
        c_min = min_cllr(fused_scores[:ntar], fused_scores[ntar:])
        logging.info('test Cxe = {}, cllr: {}, min: {}'.format(test_cxe, c, c_min))
        # plot(fused_scores);

    def test_linear_calibration(self):

        def calibrate_get_w(y, s, cstep=False):
            classf = numpy.hstack((numpy.ones(y.sum()), -numpy.ones((1-y).sum())))
            prior = 0.01
            maxiters = 100
            quiet = False
            objfun = None
            scores = numpy.hstack((s[y], s[~y]))
            fuser = LinearFuser(scores=scores)
            w0 = fuser.w
            w, train_cxe, w_pen, optimizerState, converged = train_binary_classifier(
                classifier=fuser, classf=classf, w0=w0,
                objective_function=objfun, prior=prior,
                penalizer=None, penalizer_weight=0,
                maxiters=maxiters, maxCG=100, optimizerState=None, quiet=quiet, cstepHessian=cstep)
            return w

        s1 = numpy.arange(len(y1))
        s2 = numpy.arange(len(y2))
        s3 = numpy.arange(len(y3))
        s4 = numpy.arange(len(y4))

        w1_matlab = numpy.asarray([0.05690020923651800677, -0.46189701847839187421])
        w2_matlab = numpy.asarray([-0.26721010832405112101, 1.48771909454608608492])
        w3_matlab = numpy.asarray([0.26720978993204969054, -1.71880034947314586091])
        w4_matlab = numpy.asarray([0.10852002944173323729, -1.26878113586522633938])

        w1 = calibrate_get_w(y=y1, s=s1)
        w2 = calibrate_get_w(y=y2, s=s2)
        w3 = calibrate_get_w(y=y3, s=s3)
        w4 = calibrate_get_w(y=y4, s=s4)

        self.assertTrue((numpy.abs(w1 - w1_matlab) < 1e-14).all())
        self.assertTrue((numpy.abs(w2 - w2_matlab) < 1e-14).all())
        self.assertTrue((numpy.abs(w3 - w3_matlab) < 1e-14).all())
        self.assertTrue((numpy.abs(w4 - w4_matlab) < 1e-14).all())

        w1c = calibrate_get_w(y=y1, s=s1, cstep=True)
        w2c = calibrate_get_w(y=y2, s=s2, cstep=True)
        w3c = calibrate_get_w(y=y3, s=s3, cstep=True)
        w4c = calibrate_get_w(y=y4, s=s4, cstep=True)

        self.assertTrue((numpy.abs(w1c - w1_matlab) < 1e-14).all())
        self.assertTrue((numpy.abs(w2c - w2_matlab) < 1e-14).all())
        self.assertTrue((numpy.abs(w3c - w3_matlab) < 1e-14).all())
        self.assertTrue((numpy.abs(w4c - w4_matlab) < 1e-14).all())
