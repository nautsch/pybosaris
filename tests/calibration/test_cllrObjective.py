from unittest import TestCase
from pybosaris.calibration.objectives import CllrObjective
from tests.calibration.test_objectiveFunction import test_MV2DF
from tests.test_libmath import y1, y2, y3, y4
import numpy


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"
__credits__ = ["Niko Brummer", "Edward de Villiers"]


N = 10
T = numpy.concatenate([numpy.ones(N),-numpy.ones(N),numpy.zeros(N)])
W = numpy.random.rand(3 * N)
weights = numpy.concatenate([numpy.random.rand(2*N), numpy.zeros(N)])


class TestCllrObjective(TestCase):
    def setUp(self):
        self.f = CllrObjective(T=T, weights=weights, logit_prior=-2.23)

    def test_BOSARIS(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)
