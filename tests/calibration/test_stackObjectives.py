from unittest import TestCase
from pybosaris.calibration.objectives import StackObjectives, GeneralMatrixMultiplication, LinearTransform, SubVector
from tests.calibration.test_objectiveFunction import test_MV2DF
import numpy


__author__ = "Andreas Nautsch"
__email__ = "andreas.nautsch@eurecom.fr"
__license__ = "LGPLv3"
__credits__ = ["Niko Brummer", "Edward de Villiers"]


A = numpy.random.rand(4, 5)
B = numpy.random.rand(5, 4)
W = numpy.concatenate((A.T.flatten(), B.flatten()))
T = numpy.random.rand(40, 40)
w = numpy.random.rand(10)
T1 = numpy.random.rand(11, 10)
T2 = numpy.random.rand(12, 10)


class TestStackObjectives(TestCase):
    def setUp(self):
        # f = gemm(w,4,5,4);
        # g = gemm(subvec(w,40,1,20),2,5,2);
        self.f1 = StackObjectives(
            f=GeneralMatrixMultiplication(m=4, k=5, n=4),
            g=GeneralMatrixMultiplication(m=2, k=5, n=2).compose_with(SubVector(size=40, first=0, length=20))
        )

        # f = @(w) linTrans(w,@(x)T*x,@(y)T.'*y);
        # g = @(w) gemm(w,4,5,4);
        self.f2 = StackObjectives(
            f=LinearTransform(
                map=lambda x: T @ x,
                transmap=lambda y: T.T @ y
            ),
            g=GeneralMatrixMultiplication(m=4, k=5, n=4)
        )

        # g = @(w) linTrans(w,@(x)T*x,@(y)T.'*y);
        # f = @(w) gemm(w,4,5,4);
        self.f3 = StackObjectives(
            f=GeneralMatrixMultiplication(m=4, k=5, n=4),
            g=LinearTransform(
                map=lambda x: T @ x,
                transmap=lambda y: T.T @ y
            )
        )

        # g = @(w) linTrans(w,@(x)T1*x,@(y)T1.'*y);
        # f = @(w) linTrans(w,@(x)T2*x,@(y)T2.'*y);
        self.f4 = StackObjectives(
            f=LinearTransform(
                map=lambda x: T2 @ x,
                transmap=lambda y: T2.T @ y
            ),
            g=LinearTransform(
                map=lambda x: T1 @ x,
                transmap=lambda y: T1.T @ y
            )
        )

    def test_BOSARIS_non_linear_non_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f1, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

    def test_BOSARIS_linear_non_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f2, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        # TODO idk why the deltas are worse than for the others
        self.assertTrue(test_gradient_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

    def test_BOSARIS_non_linear_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f3, W)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        # TODO idk why the deltas are worse than for the others
        self.assertTrue(test_gradient_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-6)  # 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)

    def test_BOSARIS_linear_linear(self):
        test_gradient_cstep_err, test_gradient_rstep_err, \
        test_jacobian_cstep_err, test_jacobian_rstep_err, \
        test_hessian_cstep_err, test_hessian_rstep_err, test_hessian_rcstep_err, \
        test_diff_gradient_jacobian_err, test_diff_hessian_jacobian_err = test_MV2DF(self.f4, w)
        self.assertTrue(test_gradient_cstep_err < 1e-14)
        self.assertTrue(test_jacobian_cstep_err < 1e-14)
        self.assertTrue(test_hessian_cstep_err < 1e-14)
        self.assertTrue(test_gradient_rstep_err < 1e-7)
        self.assertTrue(test_jacobian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rstep_err < 1e-7)
        self.assertTrue(test_hessian_rcstep_err < 1e-7)
        self.assertTrue(test_diff_gradient_jacobian_err == 0)
        self.assertTrue(test_diff_hessian_jacobian_err == 0)
